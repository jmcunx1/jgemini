## jgemini -- Format a Gemini Text File

Will read a
[Gemini](https://en.wikipedia.org/wiki/Gemini_(protocol))
Text File on your local system and format it in a manner similar to
[bombadillo](https://bombadillo.colorfield.space/)
or
[amfora](https://github.com/makeworld-the-better-one/amfora).


This is useful if you want to see what a
[Gemini File](https://geminiprotocol.net/)
will render as prior to uploading to the server.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jgemini) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jgemini.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jgemini.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
