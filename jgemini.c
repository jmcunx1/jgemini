/*
 * Copyright (c) 2023 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * jgemini.c -- Main routines
 */

#ifndef _MSDOS
#include <sys/param.h>
#endif
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#ifdef OpenBSD
#include <err.h>
#endif
#ifdef __FreeBSD_version
#include <err.h>
#endif
#ifdef __NetBSD_Version__
#include <err.h>
#endif

#ifdef HAVE_JLIB
#include <j_lib2.h>
#include <j_lib2m.h>
#endif

#include "jgemini.h"

/*
 * show_file_heading() -- Show run stats
 */
void show_file_heading(struct s_work *w,  struct s_counts *c, char *fname)

{

  c->writes += 3;
  fprintf(w->out.fp, "%s\n", LIT_C80);

  if (fname == (char *) NULL)
    fprintf(w->out.fp, "%s\n", LIT_STDIN);
  else
    {
      if (strcmp(fname, FILE_NAME_STDIN) == 0)
	fprintf(w->out.fp, "%s\n", LIT_STDIN);
      else
	fprintf(w->out.fp, "%s\n", fname);
    }

  fprintf(w->out.fp, "%s\n", LIT_C80);

} /* show_file_heading() */

/*
 * add_counts() -- add counts
 */
void add_counts(struct s_counts *t, struct s_counts *c)

{
  t->reads    += c->reads;
  t->writes   += c->writes;
  t->links    += c->links;
  t->headers  += c->headers;
} /* add_counts() */

/*
 * set_line() -- determine line type and add counts
 */
int set_line(struct s_counts *c, char *buf)
{
  size_t lsiz = 0;

  if (buf == (char *) NULL)
    return(LINE_TYPE_REGULAR);

  lsiz = strlen(buf);
  if (lsiz < 1)
    return(LINE_TYPE_REGULAR);

  if (strncmp(buf, TAG_LINK, 2) == 0L)
    {
      if (lsiz < 4)
	return(LINE_TYPE_REGULAR);
      c->links++;
      return(LINE_TYPE_LINK);
    }
  if (strncmp(buf, TAG_HEADER_1, 2) == 0L)
    {
      c->headers++;
      return(LINE_TYPE_HEADER);
    }
  if (strncmp(buf, TAG_HEADER_2, 3) == 0L)
    {
      c->headers++;
      return(LINE_TYPE_HEADER);
    }
  if (strncmp(buf, TAG_HEADER_3, 4) == 0L)
    {
      c->headers++;
      return(LINE_TYPE_HEADER);
    }
  if (strncmp(buf, TAG_BULLIT, 2) == 0L)
    {
      c->bullits++;
      return(LINE_TYPE_BULLIT);
    }
  if (strncmp(buf, TAG_QUOTE, 2) == 0L)
    {
      c->quotes++;
      return(LINE_TYPE_QUOTE);
    }
  if (strncmp(buf, TAG_PREFMT, 3) == 0L)
    {
      c->prefmt++;
      return(LINE_TYPE_PREFMT);
    }

  return(LINE_TYPE_REGULAR);

} /* set_line() */

/*
 * print_one_line() -- 
 */
void print_one_line(struct s_work *w, struct s_counts *c,
                    char *buf, char l1_tag, int line_adjust)
{
  int link_line_1 = FALSE;

  c->writes++;

  if (line_adjust < 1)
    {
      fprintf(w->out.fp, "%s\n", buf);
      return;
    }

  if (l1_tag == '[')
    {
      link_line_1 = TRUE;
      w->link_num++;
      fprintf(w->out.fp, "[%d]", w->link_num);
    }
  else
    fprintf(w->out.fp, "%c ", l1_tag);

  if (line_adjust < 2)
      fprintf(w->out.fp, "%s\n", ((*buf) == ' ' ? &buf[1] : buf));
  else
    {
      if (link_line_1 == TRUE)
	fprintf(w->out.fp, " %s\n", ((*buf) == ' ' ? &buf[2] : buf));
      else
	{
	  if (w->link_num > 9)
	    fprintf(w->out.fp, "   ");
	  else
	    fprintf(w->out.fp, "  ");
	  fprintf(w->out.fp, "%s\n", ((*buf) == ' ' ? &buf[2] : buf));
	}
    }

} /* print_one_line() */

/*
 * wrap_line() -- being lazy, use fmt for wrapping
 */
void wrap_line(struct s_work *w, struct s_counts *c,
               char *buf, char l1_tag, int line_adjust)
{
  size_t lsize = strlen(buf);
  size_t use_size = w->max_line - line_adjust;
  size_t i;
  long int last_space;
  char *now = buf;
  char use_tag = l1_tag;

  if (lsize <= use_size)
    {
      print_one_line(w, c, buf, l1_tag, line_adjust);
      return;
    }

  for (now = buf, i = 0, last_space = 0; now[i] != JLIB2_CHAR_NULL; i++)
    {
      if ((i < use_size) && (now[i] == ' ')) /* isspace() fails here */
	last_space = i;
      if ((isspace((int)now[i]) == 0) && (i >= use_size))
	{
	  now[last_space] = JLIB2_CHAR_NULL;
	  print_one_line(w, c, now, use_tag, line_adjust);
	  use_tag = ' ';
	  i = last_space + 1;
	  now = &now[i];
	  i = 0;
	  last_space = 0;
	}
    }
  print_one_line(w, c, now, use_tag, line_adjust);

} /* wrap_line() */

/*
 * print_line() -- print the ine based upon type
 */
void print_line(struct s_work *w, struct s_counts *c, char *buf, int line_type)
{
  char *needle = (char *) NULL;
  int i;

  switch (line_type)
    {
      case LINE_TYPE_QUOTE:
	needle = strstr(buf, " ");
	if (needle == (char *) NULL)
	  {
	    fprintf(w->out.fp, "%s\n", buf);
	    c->writes++;
	  }
	else
	  {
	    needle++;
	    wrap_line(w, c, needle, '>', 1);
	  }
	break;
      case LINE_TYPE_HEADER:
	fprintf(w->out.fp, "%s\n", buf);
	c->writes++;
	break;
      case LINE_TYPE_LINK:
	j2_bye_emb(buf);
	needle = (char *) NULL;
	for (i = 2; buf[i] != JLIB2_CHAR_NULL; i++)
	  {
	    if (buf[i] == ' ')
	      {
		if (buf[i - 1] != '>')
		  {
		    needle = &buf[i];
		    break;
		  }
	      }
	  }
	if (needle == (char *) NULL)
	  {
	    fprintf(w->out.fp, "%s\n", buf);
	    c->writes++;
	  }
	else
	  {
	    (*needle) = JLIB2_INT_NULL;
	    needle++;
	    wrap_line(w, c, needle, '[', 2);
	  }
	break;
      case LINE_TYPE_PREFMT:
	c->writes++;
	fprintf(w->out.fp, "%s\n", &buf[3]);
	break;
      case LINE_TYPE_BULLIT:
	needle = strstr(buf, " ");
	if (needle == (char *) NULL)
	  {
	    fprintf(w->out.fp, "%s\n", buf);
	    c->writes++;
	  }
	else
	  {
	    needle++;
	    wrap_line(w, c, needle, '*', 1);
	  }
	break;
      default:
	wrap_line(w, c, buf, ' ', 0);
	break;
    }

} /* print_line() */

/*
 * process_a_file()
 */
void process_a_file(struct s_work *w, struct s_counts *per, char **buf, size_t *bsize)

{

  int  line_type = 0;
  char *fmt_in  = (char *) NULL;
  char *fmt_out = (char *) NULL;
  char *expanded = (char *) NULL;
  size_t size_expanded = (size_t) 0;
  FILE *fp;

  init_counts(per);
  w->link_num = 0;

  if (w->verbose > 0)
    show_file_heading(w, per, w->arg_infile);

  if ( ! open_in(&fp, w->arg_infile, w->err.fp) )
    return;

  /*** process data ***/
  while (j2_getline(buf, bsize, fp) > (ssize_t) -1)
    {
      per->reads++;
      j2_rtw((*buf));
      if (j2_expand_tab(8, &size_expanded, &expanded, (*buf)) == FALSE)
	{
	  fprintf(w->err.fp, MSG_ERR_E003, (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
	  exit(EXIT_FAILURE);
	}
      line_type = set_line(per, expanded);
      print_line(w, per, expanded, line_type);
    }

  /*** complete ***/
  if (w->verbose > 1)
    {
      if (w->arg_infile != (char *) NULL)
	{
	  if (strcmp(w->arg_infile, FILE_NAME_STDIN) != 0)
	    fmt_in = w->arg_infile;
	}
      if (w->out.fname != (char *) NULL)
	{
	  if (strcmp(w->out.fname, FILE_NAME_STDIN) != 0)
	    fmt_out = w->out.fname;
	}
      fprintf(w->err.fp, MSG_INFO_I072, per->reads,
              (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
      fprintf(w->err.fp, MSG_INFO_I080, per->writes,
              (fmt_out == (char *) NULL ? LIT_STDOUT : fmt_out));
      fprintf(w->err.fp, MSG_INFO_I129, per->links,
              (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
      fprintf(w->err.fp, MSG_INFO_I130, per->headers,
              (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
      fprintf(w->err.fp, MSG_INFO_I131, per->quotes,
              (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
      fprintf(w->err.fp, MSG_INFO_I132, per->prefmt,
              (fmt_in  == (char *) NULL ? LIT_STDIN  : fmt_in));
    }

  close_in(&fp, w->arg_infile);

#ifdef OpenBSD
  freezero(expanded, size_expanded);
#else
  if (expanded != (char *) NULL)
    free(expanded);
#endif

} /* process_a_file() */

/*
 * process_all() -- Process all files
 */
void process_all(int argc, char **argv, struct s_work *w)

{
  int i;
  char *buf = (char *) NULL;
  size_t bsiz = (size_t) 200;
  struct s_counts totals;
  struct s_counts per_file;

  init_counts(&totals);

  /* allocate initial read buffer memory (optional) */
  buf = (char *) calloc(bsiz, sizeof(char));
  if (buf == (char *) NULL)
    {
      fprintf(w->err.fp, MSG_ERR_E080, strerror(errno));
      return;
    }

  /* process files */
  for (i = optind; i < argc; i++)
    {
      w->arg_infile = argv[i];
      process_a_file(w, &per_file, &buf, &bsiz);
      add_counts(&totals, &per_file);
    }
  if (i == optind)
    {
      w->arg_infile = (char *) NULL;
      process_a_file(w, &per_file, &buf, &bsiz);
      add_counts(&totals, &per_file);
    }

  /* show totals, no need if only 1 file processed */
  if (w->verbose > 1)
    {
      fprintf(w->err.fp, MSG_INFO_I072, totals.reads,   LIT_TOTAL);
      fprintf(w->err.fp, MSG_INFO_I080, totals.writes,  LIT_TOTAL);
      fprintf(w->err.fp, MSG_INFO_I129, totals.links,   LIT_TOTAL);
      fprintf(w->err.fp, MSG_INFO_I130, totals.headers, LIT_TOTAL);
      fprintf(w->err.fp, MSG_INFO_I131, totals.quotes,  LIT_TOTAL);
      fprintf(w->err.fp, MSG_INFO_I132, totals.prefmt,  LIT_TOTAL);
    }

#ifdef OpenBSD
  freezero(buf, bsiz);
#else
  if (buf != (char *) NULL)
    free(buf);
#endif

}  /* process_all() */

/*
 * main()
 */
int main(int argc, char **argv)

{
  clock_t tstart = clock();
  struct s_work w;

#ifdef OpenBSD
  /* proc and exec needed for popen() */
  if(pledge("stdio rpath wpath cpath proc exec",NULL) == -1)
    err(1,"pledge\n");
#endif

  init(argc, argv, &w);

  process_all(argc, argv, &w);

  if (w.verbose > 1)
    {
      fprintf(w.err.fp, MSG_INFO_I152L,
         (double)(clock() - tstart) * 1000 / (double) CLOCKS_PER_SEC);
    }

  close_out(&(w.out));
  close_out(&(w.err));
  if (w.prog_name != (char *) NULL)
    free(w.prog_name);
  exit(EXIT_SUCCESS);

}  /* main() */
