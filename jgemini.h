/*
 * Copyright (c) 2023 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef JGEMINI_H

#define JGEMINI_H "1.14 2025/03/02"
#define PROG_NAME "jgemini"

#define MAX_LINE       65
#define TAG_BULLIT     "* "
#define TAG_HEADER_1   "# "
#define TAG_HEADER_2   "## "
#define TAG_HEADER_3   "### "
#define TAG_QUOTE      "> "
#define TAG_PREFMT     "```"
#define TAG_LINK       "=>"
#define LINE_TYPE_REGULAR    0
#define LINE_TYPE_HEADER     1
#define LINE_TYPE_LINK       2
#define LINE_TYPE_BULLIT     3
#define LINE_TYPE_QUOTE      4
#define LINE_TYPE_PREFMT     5

#ifndef JLIB2_CHAR_NULL
#define NO_JLIB 1
#define JLIB2_INT_NULL   ((int) '\0')
#define JLIB2_CHAR_NULL  ((char) '\0')
#define JLIB2_UCHAR_NULL ((unsigned char) '\0' )
#endif

#ifndef NULL
#define NULL '\0'
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#ifndef SSIZE_T
#define SSIZE_T ssize_t
#endif
#ifndef TAB_CHAR
#define TAB_CHAR 0x09
#endif

#ifdef MAXPATHLEN
#ifndef PATH_MAX
#define PATH_MAX MAXPATHLEN
#endif
#else
#ifdef PATH_MAX
#define MAXPATHLEN PATH_MAX
#endif
#endif

/*** structures ***/
struct s_file_info
{
  FILE *fp;
  char *fname;
} ;
struct s_counts
{
  long int reads;
  long int writes;
  long int links;
  long int headers;
  long int bullits;
  long int quotes;
  long int prefmt;
} ;

struct s_work
{
  struct s_file_info out;         /* default stdout            */
  struct s_file_info err;         /* default stderr            */
  char *prog_name;                /* real program name         */
  int verbose;                    /* TRUE or FALSE             */
  int force;                      /* TRUE or FALSE             */
  int max_line;                   /* max line length to print  */
  int link_num;                   /* link number for display   */
  char *arg_infile;               /* Input file from Arguments */
} ;

/*** messages ***/
#ifdef NO_JLIB
#define ARG_ERR           'e'  /* Output Error File                  */
#define ARG_FORCE         'f'  /* force create files                 */
#define ARG_HELP          'h'  /* Show Help                          */
#define ARG_MAX_SIZE      'M'  /* Maximum Size                       */
#define ARG_OUT           'o'  /* Output File                        */
#define ARG_VERBOSE       'v'  /* Verbose                            */
#define ARG_VERSION       'V'  /* Show Version Information           */
#define FILE_NAME_STDIN   "-"
#define FILE_NAME_STDOUT  "-"
#define LIT_C80           "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
#define LIT_INFO_04       "Build: %s %s\n"
#define LIT_REV           "Revision"
#define LIT_STDIN         "(standard input)"
#define LIT_STDOUT        "(standard output)"
#define LIT_READ          "read"
#define LIT_TOTAL         "Total"
#define MSG_ERR_E000      "Try '%s %c%c' for more information\n"
#define MSG_ERR_E002      "ERROR E002: Cannot open '%s' for write, processing aborted\n"
#define MSG_ERR_E003      "ERROR E003: Cannot allocate memory when processing file %s, processing aborted\n"
#define MSG_ERR_E005      "ERROR E005: need one or more files to process\n"
#define MSG_ERR_E008      "ERROR E008: '%s' is an invalid value for %c%c, must be numeric\n"
#define MSG_ERR_E010      "ERROR E010: cannot open %s for %s\n"
#define MSG_ERR_E025      "ERROR E025: File %s cannot be created, already exists\n"
#define MSG_ERR_E074      "ERROR E074: 'Too many Arguments specified for %c%c\n"
#define MSG_ERR_E080      "ERROR E080: cannot allocate initial memory : %s\n"
#define MSG_INFO_I072     "I072:    Lines Reads:  %9ld - File %s\n"
#define MSG_INFO_I080     "I080:   Lines Writes:  %9ld - File %s\n"
#define MSG_INFO_I096     "I096:     Read Bytes:  %9ld - File %s\n"
#define MSG_INFO_I097     "I097:    Write Bytes:  %9ld - File %s\n"
#define MSG_INFO_I129     "I129:    Links Found:  %9ld - File %s\n"
#define MSG_INFO_I130     "I130:  Headers Found:  %9ld - File %s\n"
#define MSG_INFO_I131     "I131:   Quotes Found:  %9ld - File %s\n"
#define MSG_INFO_I132     "I132:  pre-fmt Found:  %9ld - File %s\n"
#define MSG_INFO_I152L    "I152:       Run Time:  %f ms\n"
#define MSG_WARN_W002     "W002: Open Error Bypass File '%s' : %s\n"
#define SWITCH_CHAR       '-'
#define USG_MSG_ARG_ERR         "\t%c%c file\t\t: Write errors to file 'file', default stderr\n"
#define USG_MSG_ARG_FORCE       "\t%c%c\t\t: force create of files when found\n"
#define USG_MSG_ARG_HELP        "\t%c%c\t\t: Show brief help and exit\n"
#define USG_MSG_ARG_MAX_SIZE_3  "\t%c%c n\t\t: Maximum Size of Print Line, default %d\n"
#define USG_MSG_ARG_OUT         "\t%c%c file\t\t: Write output to file 'file', default stdout\n"
#define USG_MSG_ARG_VERBOSE_8   "\t%c%c\t\t: verbose level, each time specified level increases\n"
#define USG_MSG_ARG_VERSION     "\t%c%c\t\t: Show revision information and exit\n"
#define USG_MSG_OPTIONS         "Options\n"
#define USG_MSG_USAGE           "usage:\t%s [OPTIONS] [FILES ...]\n"
#endif /* NO_JLIB */

/*** prototypes ***/
void init(int, char **, struct s_work *);
void init_finfo(struct s_file_info *);
void show_brief_help(FILE *, char *);
void show_rev(FILE *, char *);
void process_arg(int, char **, struct s_work *);
int  open_out(FILE *, struct s_file_info *, char *, int);
void close_out(struct s_file_info *);
int  open_in(FILE **, char *, FILE *);
void close_in(FILE **, char *);
void init_counts(struct s_counts *c);

#ifdef NO_JLIB
int j2_f_exist(char *file_name);
long int j2_rtw(char *buffer);
char *j2_get_prgname(char *argv_0, char *default_name);
SSIZE_T j2_getline(char **buf, size_t *n, FILE *fp);
long int j2_clr_str(char *s, char c, int size);
int j2_is_numr(char *s);
int j2_expand_tab(int tab_size, size_t *out_buf_size, char **out_buf, char *in_buf);
long int j2_chg_char(char old, char new, char *s, SSIZE_T force_size);
long int j2_bye_emb(char *s);
#endif /* NO_JLIB */

#endif /*  JGEMINI_H  */

/* END: jgemini.h */
