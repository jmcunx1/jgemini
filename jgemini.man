.\"
.\" Copyright (c) 2023 2024 2025
.\"     John McCue
.\"
.\" Permission to use, copy, modify, and distribute this software
.\" for any purpose with or without fee is hereby granted,
.\" provided that the above copyright notice and this permission
.\" notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
.\" WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
.\" WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
.\" FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
.\" SOFTWARE.
.\"
.TH JGEMINI 1 "2023-09-15" "JMC" "User Commands"
.SH NAME
jgemini - Format a Gemini File for Display
.SH SYNOPSIS
jgemini [OPTIONS] [FILE...]
.SH DESCRIPTION
Reads a Gemini File and formats for display in a
similar manner to bombadillo or amfora.
.PP
If no files (FILE) are specified on the command line or
if FILE has name '-', stdin (Standard Input) is used.
.TP
-e file
Optional, if used, write error messages to file 'file'.
If not specified, errors written to stderr.
.TP
-f
Optional, Force file create.
Create file even if the target file exists.
Default, abort if Output File(s) exist.
.TP
-h
Show brief help and exit.
.TP
-M n
Set the Maximum Length of each line printed.
Default 65 characters.
.TP
-o file
Optional, if used, write Output to File 'file'.
If not specified, output written to stdout.
.TP
-V
Output version information and exit.
.TP
-v
Optional, Verbose Level.
Print information about the run,
default do not show run messages.
Can be specified multiple times,
each specification increases verbose level:
.nf
    Level  Meaning
    -----  --------------------------------------------
    = 0    Only show error messages on stderr, default.
    >= 1   Above plus Show Headings on stdout as each
           file is processed.
    >= 2   Above plus show summary counts and warnings
           on stderr.
.fi
.SH DIAGNOSTICS
If you specify '-e', in some cases info will still be
written to stderr.
This can occur during Command Line Argument Processing.
.PP
This only formats a file to display on your local
terminal.
It is far from a Gemini Client.
.PP
I believe UTF-8 characters should work fine,
but I have no explicit logic for UTF-8.
.SH SEE-ALSO
amfora(1),
awk(1),
bombadillo(1),
cat(1),
castor(1),
cut(1),
fmt(1),
head(1),
jcat(1),
lagrange(1),
less(1),
paste(1),
pg(1),
stdin(4),
stdout(4),
stderr(4),
tail(1)
.SH ERROR-CODES
.nf
0 success
1 processing error or help/rev displayed
.fi
